#!/usr/bin/env python
"""Tests for `thermo_skanit7` package."""
# pylint: disable=redefined-outer-name
from thermo_skanit7 import __version__
from thermo_skanit7.thermo_skanit7_interface import GreeterInterface
from thermo_skanit7.thermo_skanit7_impl import HelloWorld

def test_version():
    """Sample pytest test function."""
    assert __version__ == "0.0.1"

def test_GreeterInterface():
    """ testing the formal interface (GreeterInterface)
    """
    assert issubclass(HelloWorld, GreeterInterface)

def test_HelloWorld():
    """ Testing HelloWorld class
    """
    hw = HelloWorld()
    name = 'yvain'
    assert hw.greet_the_world(name) == f"Hello world, {name} !"

