"""Top-level package for Thermo SkanIt 7."""

__author__ = """mark doerr"""
__email__ = "mark.doerr@uni-greifswald.de"
__version__ = "0.0.1"
