# Thermo SkanIt 7

Thermo Scientific SkanIT 7 SiLA server / client for plate readers like VarioSkan Lux

## Features

## Installation

    pip install thermo_skanit7 --index-url https://gitlab.com/api/v4/projects/<gitlab-project-id>/packages/pypi/simple

## Usage

    thermo_skanit7 --help 

## Development

    git clone gitlab.com/opensourcelab/thermo-skanit7

    # create a virtual environment and activate it then run

    pip install -e .[dev]

    # run unittests

    invoke test   # use the invoke environment to manage development
    

## Documentation

The Documentation can be found here: [https://opensourcelab.gitlab.io/thermo-skanit7](https://opensourcelab.gitlab.io/thermo-skanit7) or [thermo-skanit7.gitlab.io](thermo_skanit7.gitlab.io/)


## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter)
 and the [gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage](https://gitlab.com/opensourcelab/software-dev/cookiecutter-pypackage) project template.



